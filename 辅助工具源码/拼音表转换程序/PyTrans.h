// PyTrans.h : main header file for the PyTrans application
//

#if !defined(AFX_TEST_H__EA17C0DE_F471_4C7D_B233_BBE36E2AD541__INCLUDED_)
#define AFX_TEST_H__EA17C0DE_F471_4C7D_B233_BBE36E2AD541__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CPytransApp:
// See PyTrans.cpp for the implementation of this class
//

class CPyTransApp : public CWinApp
{
public:
	CPyTransApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPyTransApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CPyTransApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEST_H__EA17C0DE_F471_4C7D_B233_BBE36E2AD541__INCLUDED_)
