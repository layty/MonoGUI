// DlgShowButtons.h
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined (__DLGSHOWBUTTONS_H__)
#define __DLGSHOWBUTTONS_H__

class CDlgShowButtons : public ODialog
{
public:
	CDlgShowButtons();
	virtual ~CDlgShowButtons();

	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, int nMsg, int wParam, int lParam);

private:
	int m_nCurrentImageIndex;
};

#endif // !defined(__DLGSHOWBUTTONS_H__)