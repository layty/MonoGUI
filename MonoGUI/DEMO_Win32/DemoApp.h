// DemoApp.h: interface for the DemoApp class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined (__DEMOAPP_H__)
#define __DEMOAPP_H__

class DemoApp : public OApp
{
	int  m_nSysMode;             // 当前模式
	int  m_nCurLockPos;          // 当前锁位
	BOOL m_bLockChangeEnable;    // 是否允许改变当前锁位

public:
	DemoApp();
	virtual ~DemoApp();

	// 绘制
	virtual void PaintWindows (OWindow* pWnd);

	// 显示开机画面
	virtual void ShowStart();
};

#endif // !defined (__DEMOAPP_H__)
