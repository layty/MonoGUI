// DlgShowProgress.cpp
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
//////////////////////////////////////////////////////////////////////
CDlgShowProgress::CDlgShowProgress()
{
}

CDlgShowProgress::~CDlgShowProgress()
{
}

// 初始化
void CDlgShowProgress::Init()
{
	m_pApp->PaintWindows (m_pApp->m_pMainWnd);
	OProgressBar* pPgs = (OProgressBar *)FindChildByID (102);
	pPgs->SetRange (100);
	int i;
	for (i = 0; i <= 100; i++)
	{
		pPgs->SetPos (i);

		// 延时一会儿
		Delay(10);
	}
	// 延时一会儿
	Delay(500);

	// 关闭本窗口
	O_MSG msg;
	msg.pWnd = this;
	msg.message = OM_CLOSE;
	msg.wParam = 0;
	msg.lParam = 0;
	m_pApp->PostMsg (&msg);
	m_pApp->CleanKeyBuffer();
}

/* END */